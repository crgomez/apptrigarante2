import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicationSettings } from '@nativescript/core';
import { Observable } from 'rxjs';
import { environment } from '~/environment/environment';
import { DataLoginModel } from '../../Models/Login/DataLoginModel';
import { LoginModel } from '../../Models/Login/LoginModel';

@Injectable({ providedIn: 'root' })
export class AuthService {

    private userActive: number;

    constructor(
        private http: HttpClient
    ) { }

    LogIn(userData: LoginModel): Observable<DataLoginModel> {
        return this.http.post<DataLoginModel>(`${environment.apiNode}/login`, userData);
    }

    HasUserActive(): boolean {
        if (ApplicationSettings.getNumber('userActive')) {
            this.userActive = ApplicationSettings.getNumber('userActive');
            if (this.userActive > -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}