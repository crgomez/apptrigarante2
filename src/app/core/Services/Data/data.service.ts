import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '~/environment/environment';
import { DataClientModel } from '../../Models/Client/DataClientModel';

@Injectable({ providedIn: 'root' })
export class DataService {

    constructor(
        private http: HttpClient
    ) { }

    getDataClient(id: number): Observable<DataClientModel> {
        return this.http.get<DataClientModel>(`${environment.apiMark}/v1/cliente/${id}`);
    }

}
