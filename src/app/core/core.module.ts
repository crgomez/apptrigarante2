import { NgModule } from '@angular/core';
import { AuthService } from './Services/Auth/auth.service';
import { DataService } from './Services/Data/data.service';
import { PoliciesService } from './Services/Policies/policies.service';

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers: [
        AuthService,
        DataService,
        PoliciesService
    ]
})
export class CoreModule { }
