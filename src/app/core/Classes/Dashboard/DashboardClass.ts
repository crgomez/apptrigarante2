import { ApplicationSettings } from "@nativescript/core";
import { DataClientModel } from "../../Models/Client/DataClientModel";
import { DataService } from "../../Services/Data/data.service";
import { SqliteMethodsClass } from "../Sqlite/SqliteMethodsClass";

export class DashboardClass {

    private sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();

    userActive: number;
    fullName: string;

    constructor(
        private dataService: DataService
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.getDataClientAPI();
    }

    async getDataClient() {
        const fields: string = `*`;
        const tableName: string = `CLIENTES`;
        const clausule: string = `WHERE ID = ${this.userActive}`;

        let clientDataArr: Array<string>;
        await this.sqliteMethods.getData(fields, tableName, clausule)
            .then((result: any) => {
                if (result.length > 0) {
                    clientDataArr = result;
                    this.fullName = `${clientDataArr[0][1]} ${clientDataArr[0][2]} ${clientDataArr[0][3]}`;
                }
            });

        return this.fullName;
    }

    private async getDataClientAPI() {
        this.dataService.getDataClient(this.userActive)
            // tslint:disable-next-line: deprecation
            .subscribe((data: DataClientModel) => {
                console.log('Data from API => ', data);
                this.fullName = `${data.nombre} ${data.paterno} ${data.materno}`;
                this.updateDataDashboard(data, this.userActive);
            });
    }

    updateDataDashboard(data: DataClientModel, userActive: number) {
        console.log('Entro a updateDataDashboar');
        const fields: string = ``;
        const tableName: string = `CLIENTES`;
        const clausule: string = `WHERE ID = ${userActive}`;

        const dataClient = [];
        dataClient.push({ name: `NOMBRE`, value: `'${data.nombre}'` });
        dataClient.push({ name: `A_PATERNO`, value: `'${data.paterno}'` });
        dataClient.push({ name: `A_MATERNO`, value: `'${data.materno}'` });
        dataClient.push({ name: `FECHA_NACIMIENTO`, value: `'${data.fechaNacimiento}'` });
        dataClient.push({ name: `CURP`, value: `'${data.curp}'` });
        dataClient.push({ name: `RFC`, value: `'${data.rfc}'` });
        dataClient.push({ name: `TELEFONO_FIJO`, value: `'${data.telefonoFijo}'` });
        dataClient.push({ name: `TELEFONO_MOVIL`, value: `'${data.telefonoMovil}'` });
        dataClient.push({ name: `CORREO`, value: `'${data.correo}'` });
        dataClient.push({ name: `PAIS`, value: `'${data.nombrePaises}'` });
        dataClient.push({ name: `COLONIA`, value: `'${data.colonia}'` });
        dataClient.push({ name: `CP`, value: `'${data.cp}'` });
        dataClient.push({ name: `CALLE`, value: `'${data.calle}'` });
        dataClient.push({ name: `NUM_EXT`, value: `'${data.numExt}'` });
        dataClient.push({ name: `NUM_INT`, value: `'${data.numInt}'` });
        this.sqliteMethods.updateDataClient(tableName, dataClient, clausule)
            .then((result) => {
                console.log("Status => ", result);
            })
            .catch((error) => {
                console.error("Error UpdateData => ", error);
            });

    }
}
