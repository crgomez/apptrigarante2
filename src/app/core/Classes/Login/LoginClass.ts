import { ApplicationSettings } from "@nativescript/core";
import { DataLoginModel } from "../../Models/Login/DataLoginModel";
import { LoginModel } from "../../Models/Login/LoginModel";
import { AuthService } from "../../Services/Auth/auth.service";
import { SqliteMethodsClass } from "../Sqlite/SqliteMethodsClass";
import { SqliteTables } from "../Sqlite/SqliteTablesModel";

export class LoginClass {

    isDataLoaded: boolean = false;
    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    sqliteTables: SqliteTables = new SqliteTables();
    // responseData: DataLoginModel;

    // El constructar recibe la instancia del servicio Auth para poder acceder al método y este no
    // marque error de null
    constructor(
        private authService: AuthService
    ) { }

    // Método que hará el proceso de logueo, enviar y recibir información del API
    processLogIn(userData: LoginModel): void {
        let responseData: DataLoginModel;
        this.authService.LogIn(userData)
            // tslint:disable-next-line: deprecation
            .subscribe((data: DataLoginModel) => {
                if (data !== null) {
                    responseData = data;
                    this.validateSession(data);
                }
            }, (error => {
                console.log(error);
            }), () => {
                ApplicationSettings.setNumber("userActive", Number(responseData.id));
                this.isDataLoaded = true;
            });
    }

    private validateSession(responseData: DataLoginModel) {
        if (responseData.id > -1) {
            const fields = `ID_CLIENTE`;
            const tableName = `USUARIOS`;
            const clausule = `WHERE ID_CLIENTE = ${responseData.id}`;
            this.sqliteMethods.getData(fields, tableName, clausule)
                .then((_response) => {
                    if (_response.length > 0) {
                        this.sqliteMethods.updateDataClient('USUARIOS', [{ name: 'ACCESO', value: String(responseData.tipo) }], `WHERE ID_CLIENTE = ${responseData.id}`);
                    } else {
                        this.updateDataClient(responseData);
                    }
                })
                .catch((error: any) => {
                    console.log(`Error en sesión validate => ${error.message}`);
                });
        }
    }

    private updateDataClient(responseData: DataLoginModel) {
        console.log(`Update Cliente SessionClass`);
        const dataArray = [{ name: "ACCESO", value: String(responseData.tipo) }];
        const tablesName = this.sqliteTables.tablesName;
        const tableName = `USUARIOS`;
        const cluasule = `WHERE ID_CLIENTE = ${responseData.id}`;

        /* this.sqliteMethods.updateDataClient(tableName, dataArray, cluasule)
            .then((result: any) => {
                console.log("Result Update 1", result);
                if (result) { */
        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < tablesName.length; index++) {
            this.sqliteMethods.deleteDataClient(tablesName[index])
                .then((resultDelete: any) => {
                    console.log('result delete', resultDelete);
                    if (index === (tablesName.length - 1)) {
                        let dataUserArray = [{ name: "ID", value: String(responseData.id) }];
                        this.sqliteMethods.setDataClient("CLIENTES", dataUserArray)
                            .then(() => {
                                dataUserArray = [
                                    { name: "ID_CLIENTE", value: String(responseData.id) },
                                    { name: "ACCESO", value: String(responseData.tipo) }
                                ];
                                this.sqliteMethods.setDataClient("USUARIOS", dataUserArray);
                            });
                    }
                }).catch((error) => {
                    console.log("\nError en delete tables => ", error.message);
                });
        }
        /*    }
         })
        .catch((error: any) => {
            console.log(`Error update data client SessionClass => `, error.message);
        }); */
    }

}
