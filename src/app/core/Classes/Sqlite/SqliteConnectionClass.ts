import * as fs from "@nativescript/core/file-system";

const sqlite = require('nativescript-sqlite');

export class SqliteConnectionClass {
    /* Esta clase ha sido diseñada con base en el patrón de diseño SONGLETON con la finalidad de no
    estar creando instancias innecesarias y así obligar a usar la instancia existente sin necesidad de
    volver a a crear una nueva, sino solamente retornar la ya existente y evitar la saturación
    de conexiones a la base de datos */

    /* Este método estático es el que crea la instancia de la clase y reusar la misma en caso de que
    ya existena una y así evitar usar la palabra reservada new */

    static getClassInstance(): SqliteConnectionClass {
        /* Primero ser verifica si la variable que almacenará la instancia es nula, si es sí, entonces
        creará la instancia y al final retornará la misma; caso contrario si no es nula, lo único que
        hará es retorarnla ya que esta se encuentra abierta. */
        if (SqliteConnectionClass.singletonInstance === null) {
            // Instancia la clase cuando la variable es nula
            SqliteConnectionClass.singletonInstance = new SqliteConnectionClass();
        }

        // Retorna la instancia de la clase
        return SqliteConnectionClass.singletonInstance;
    }

    // A través de esta variable es que se podrá acceder a los métodos publicos de la clase
    private static singletonInstance: SqliteConnectionClass = null;

    // A través de esta variable es que se podrán realizar las operaciones a la base de datos.
    private static connectionInstance = null;

    // Nombre de la base de datos
    private databaseName: string = "db_trigarante.db";

    /* El constructor se vuelve privado para evitar instanciar la clase y obligar a usar el método estático
    getClassInstance() */
    private constructor() { }

    /* Primero verifica si la variable que almacenará la instancia de la base de datos es nula, si es sí,
    entonces creará una instancia y la retornará, caso contrario, solo retornará la instancia ya que esta
    se encuentra abierta.
    Es un método asíncrono ya que retornaba la isntancia y su valor era nulo y por tal razón ocurría un error
    al intentar ejecutar el método execSQL, ahora primero resulve el método y al final retorna el resultado*/
    async getConnectionInstance(): Promise<any> {
        if (SqliteConnectionClass.connectionInstance === null) {
            // Espera que tener una respuesta y poder continuar ejecutando el código
            await new sqlite(this.databaseName)
                .then((db: any) => {
                    console.log('DB => ', db);
                    // Se almacena la instancia de la db en la variable que se reutilizará
                    SqliteConnectionClass.connectionInstance = db;
                });
        }

        // Retorna la variable con la cual se tendrá acceso a la base de datos
        return SqliteConnectionClass.connectionInstance;

    }

    // Método para cerrar la instancia de la clase
    closeClassInstance() {
        SqliteConnectionClass.singletonInstance = null;
    }

    // Método para cerrar la conexión a la base de datos y volver nula la instancia
    /* closeConnectionInstance() {
        SqliteConnectionClass.connectionInstance.close();
        SqliteConnectionClass.connectionInstance = null;
    } */

}