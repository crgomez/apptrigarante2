import { ApplicationSettings } from "@nativescript/core";
import { SqliteMethodsClass } from "../Sqlite/SqliteMethodsClass";

export class PersonalInformationClass {

    private userActive: number;
    private sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    private dataClient: Array<[]>;

    constructor() {
        this.userActive = ApplicationSettings.getNumber('userActive');
    }

    async loadDataClient() {
        let client: Array<[]>
        const fields: string = `*`;
        const tableName: string = `CLIENTES`;
        const clausule: string = `WHERE ID = ${this.userActive}`
        await this.sqliteMethods.getData(fields, tableName, clausule)
            .then((result) => {
                client = result[0];
            })
            .catch((error) => {
                console.log('Error Result Persona Information => ', error.message);
            })
            .finally(() => {
                this.dataClient = client;
            });

        return this.dataClient;
    }
}