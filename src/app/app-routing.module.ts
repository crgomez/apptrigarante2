import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { LayoutComponent } from "./components/layout/layout.component";

import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
    {
        path: "",
        component: LayoutComponent,
        children: [
            {
                path: "",
                redirectTo: "/dashboard",
                pathMatch: "full"
            },
            {
                path: "dashboard",
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/dashboard/dashboard.module').then((m) => m.DashboardModule)
            },
            {
                path: "login",
                loadChildren: () => import('./components/login/login.module').then((m) => m.LoginModule)
            }
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
