import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import { Application } from "@nativescript/core";
import { SqliteMethodsClass } from "./core/Classes/Sqlite/SqliteMethodsClass";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();

    constructor() {
        // Use the component constructor to inject services.
    }

    ngOnInit(): void {
        this.sqliteMethods.createTablesSqlite();
    }
}
