import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';
import { Observable } from 'rxjs';
import { AuthService } from '../core/Services/Auth/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(
        private authService: AuthService,
        private routerExtensions: RouterExtensions
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        // Se valida que haya un usuario activo en la configuración de la aplicación
        if (this.authService.HasUserActive()) {
            // Si la varible existe se retorna true
            return true;
        } else {
            this.routerExtensions.navigate(['/login'], {
                transition: {
                    name: 'fade'
                },
                clearHistory: true
            });
            return false;
        }
    }
}