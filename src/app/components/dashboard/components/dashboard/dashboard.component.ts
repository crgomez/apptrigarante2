import { Component, OnInit } from '@angular/core';
import { ApplicationSettings } from '@nativescript/core';
import { DashboardClass } from '~/app/core/Classes/Dashboard/DashboardClass';
import { SqliteMethodsClass } from '~/app/core/Classes/Sqlite/SqliteMethodsClass';
import { DataClientModel } from '~/app/core/Models/Client/DataClientModel';
import { DataService } from '~/app/core/Services/Data/data.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

    //#region Variables

    fullClientName: string = "";
    sqliteMethods: SqliteMethodsClass = new SqliteMethodsClass();
    dashboardClass: DashboardClass = new DashboardClass(this.dataService);
    dataLoadeComplete: boolean = false;
    userActive: number;

    //#endregion Variables

    constructor(
        private dataService: DataService
    ) {
        this.getDataClient();
    }

    ngOnInit(): void { }

    LogOut(): void { }

    private getDataClient(): void {
        this.dashboardClass.getDataClient()
        .then(data => {
            this.fullClientName = data;
        });
    }

}