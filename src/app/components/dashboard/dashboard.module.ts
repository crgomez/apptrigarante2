import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from '~/app/shared/shared.module';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DashboardRoutingModule,
        SharedModule
    ],
    exports: [],
    declarations: [DashboardComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule { }
