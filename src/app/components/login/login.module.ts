import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptCommonModule, NativeScriptFormsModule } from '@nativescript/angular';
import { SharedModule } from '~/app/shared/shared.module';

import { LoginComponent } from './components/login/login.component';

import { LoginRoutingModule } from './login-routing.module';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        LoginRoutingModule,
        ReactiveFormsModule,
        NativeScriptFormsModule,
        SharedModule
    ],
    exports: [],
    declarations: [LoginComponent],
    providers: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule { }
