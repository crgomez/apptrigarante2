import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { Page } from '@nativescript/core';
import { LoginClass } from '~/app/core/Classes/Login/LoginClass';
import { AuthService } from '~/app/core/Services/Auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {

    message: string = 'Hola, este es un mensaje';
    isLogin: boolean = false;
    formGroup: FormGroup
    isShowingPassword: boolean = false;
    textLoader: string;

    // Se inicia una instancia de la clase donde se hará todo el proceso que conlleva el logueo
    loginClass: LoginClass;

    constructor(
        private page: Page,
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private routerExtensions: RouterExtensions
    ) {
        this.page.actionBarHidden = true;
        this.loginClass = new LoginClass(this.authService);
    }

    ngOnInit(): void {
        this.startBuildForm();
    }

    // Método que se ejecuta cuando se da clic en el ícono del ojo que se encuentra en el TextField Contraseña
    showPassword(): void {
        // Su función es mostrar u ocultar la contraseña dependiendo del estado de la viarable que se
        // está usando.
        this.isShowingPassword = !this.isShowingPassword;
    }

    // Método que se encargará de hacer el logueo
    logIn() {
        // Primero se valida que el formulario tenga todos los valores
        if (this.formGroup.valid) {
            // Se vuelve true para que se muestre el loader
            this.isLogin = true;
            // Se manda a llamar al método el cual hará todo el proceso de inicio de sesión
            // y se envía como parámetros, todo el formulario.
            this.loginClass.processLogIn(this.formGroup.value);
            // Se le coloca un texto para indicar al lo que se está haciendo
            this.textLoader = "Validando Inicio de sesión";
            // Se agrega un timeOut para esperar un tiempo y proseguir con las demás instrucciones
            setTimeout(() => {
                // Se valida si la vaiable isDataLoaded es true la cual indica que la información
                // traía desde el servidor a través de la api y el servicio ya está completamente
                // cargada.
                if (this.loginClass.isDataLoaded) {
                    // Si la viable es true, se redireccionará al dashboard o página principal de la app
                    this.reditecTo();
                } else if (!this.loginClass.isDataLoaded) {
                    // Si la variable es false se le indicará que ha ocurrido un error
                    this.textLoader = "Sus datos son incorrectos.";
                    // Se agrega otro timeOut para cerrar el loader y no hacerlo de manera inmedienta
                    // ya que no se mostraría el texto y el usuario no leería cual es el error.
                    setTimeout(() => {
                        this.isLogin = false;
                    }, 500);
                }
            }, 1500);
        }
    }

    // Método que se encarga de redireccionar al dashboard o página principal de la aplicación
    // una vez haya sido aprobado el inicio de sesión
    private reditecTo() {
        // Se coloca un texto para indicar al usuario que su sesión a sido aprobada y que está iniciando
        this.textLoader = "Inciando sesión";
        // Se agrega un timeOut para que no se cierre inmediatamente el loader y puedo observar el usuario,
        // y después se le redirija a la página correspondiente.
        setTimeout(() => {
            this.isLogin = false;
            this.routerExtensions.navigate(['/dashboard'], {
                transition: {
                    name: 'fade'
                },
                clearHistory: true
            });
        }, 1000);
    }

    // Método que carga el formGroup y hacer uso de él a través de la directiva FormGroup en el html,
    // y acceder asignarlos a los TextField a través de la propiedad formControlName.
    private startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            email: ["", [Validators.required, Validators.pattern("^[^@]+@[^@]+\.[a-zA-Z]{2,}$")]],
            contrasenaApp: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(32)]]
        });
    }

    //#region getProperties

    // Propiedades get que se usan para acceder a la propiedad del mismo nombre que se encuentra en el
    // formgroup pero simplificando, esto para realizar las validaciones y comprobar las mismas de manera
    // más fácil y más sencilla.
    get emailField(): AbstractControl {
        return this.formGroup.get('email');
    }

    get passwordField(): AbstractControl {
        return this.formGroup.get('contrasenaApp');
    }

    //#endregion getProperties
}