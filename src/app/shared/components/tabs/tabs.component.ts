import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ApplicationSettings, GridLayout, Page, PanGestureEventData } from '@nativescript/core';
import * as screen from '@nativescript/core/platform';
import { AnimationCurve } from '@nativescript/core/ui/enums';
import { SelectedIndexChangedEventData } from '@nativescript/core/ui/tabs';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.component.html',
    styleUrls: ['tabs.component.scss']
})

export class TabsComponent implements /* OnInit */ AfterViewInit {

    @ViewChild('tabs', { static: true }) tabs: ElementRef;
    @ViewChild('centerCircle', { static: true }) centerCircle: ElementRef;
    @ViewChild('dragCircle', { static: true }) dragCircle: ElementRef;
    @ViewChild('leftTabs', { static: true }) leftTabs: ElementRef;
    @ViewChild('rightTabs', { static: true }) rightTabs: ElementRef;
    @ViewChild('centerPatch', { static: true }) centerPatch: ElementRef;
    @ViewChild('tabBGContainer', { static: true }) tabBGContainer: ElementRef;

    @ViewChildren('tabContents', { read: ElementRef }) tabContents: QueryList<ElementRef>;

    // Auxiliar de Pan (Pan Helper)
    prevDeltaX: number = 0;

    animationCurve = AnimationCurve.cubicBezier(0.38, 0.47, 0, 1);

    // Contenido de las pestañas y color de fondo
    tabContainer = {
        backgroundColor: '#EFEFEF',
        focusColor: '#FFF'
    };

    tabList: Array<{ text: string, icon?: string, color?: string, backgroundColor: string, fadeColor?: string }> = [
        { text: String.fromCharCode(0xf07c), backgroundColor: '#FFF', color: '#000' }, // Pólizas
        { text: String.fromCharCode(0xf15c), backgroundColor: '#FFF', color: '#000' }, // Solicitudes
        { text: String.fromCharCode(0xf015), backgroundColor: '#FFF', color: '#000' }, // Home
        { text: String.fromCharCode(0xf007), backgroundColor: '#FFF', color: '#000' }, // Perfil
        { text: String.fromCharCode(0xf129), backgroundColor: '#FFF', color: '#000' } // Info
    ];


    currentTabIndex: number = 2;
    defaultSelected: number = 2;

    constructor(private page: Page) {
        this.page.actionBarHidden = true;
    }

    /* ngOnInit() { } */
    ngAfterViewInit(): void {
        this.initializeTabBar();
    }

    // Interacción del usuario

    // El ínidice de las pestañas seleccionado cambió, por ejemplo, cuando deslice para navegar
    onSelectedIndexChanged(args: SelectedIndexChangedEventData): void {
        if (args.newIndex !== this.currentTabIndex) {
            this.onBottomNavTap(args.newIndex);
        }
    }

    onBottomNavTap(index: number, duration: number = 300): void {
        if (this.currentTabIndex !== index) {
            const tabContentsArr = this.tabContents.toArray();
            // Establecer el foco en el índice anterior
            tabContentsArr[this.currentTabIndex].nativeElement.animate(this.getUnfocusAnimation(this.currentTabIndex, duration));
            // Establecer el foco en el índice actual
            tabContentsArr[index].nativeElement.animate(this.getFocusAnimation(index, duration));
        }

        // cambiar el índice de pestañas seleccionado cuando toca en la tira de pestañas
        if (this.tabs.nativeElement.selectedIndex !== index) {
            this.tabs.nativeElement.selectedIndex = index;
        }

        this.centerCircle.nativeElement.animate(this.getSlideAnimation(index, duration));
        this.leftTabs.nativeElement.animate(this.getSlideAnimation(index, duration));
        this.rightTabs.nativeElement.animate(this.getSlideAnimation(index, duration));
        this.centerPatch.nativeElement.animate(this.getSlideAnimation(index, duration));
        this.dragCircle.nativeElement.animate(this.getSlideAnimation(index, duration));

        // Colocar el índice actual en el índice nuevo
        this.currentTabIndex = index;
    }

    onCenterCirclePan(args: PanGestureEventData): void {
        const grdLayout: GridLayout = <GridLayout>args.object;
        const newX: number = grdLayout.translateX + args.deltaX - this.prevDeltaX;

        if (args.state === 0) {
            // Dedo abajo
            this.prevDeltaX = 0;
        } else if (args.state === 2) {
            // Movimientos de los dedos
            grdLayout.translateX = newX;
            this.leftTabs.nativeElement.translateX = newX;
            this.rightTabs.nativeElement.translateX = newX;
            this.centerPatch.nativeElement.translateX = newX;
            this.centerCircle.nativeElement.translateX = newX;

            this.prevDeltaX = args.deltaX;
        } else if (args.state === 3) {
            // Dedo arriba
            this.prevDeltaX = 0;
            const tabWidth = screen.Screen.mainScreen.widthDIPs / this.tabList.length;
            const tabSelected: number = Math.round(Math.abs(newX / tabWidth));
            const translateX: number = tabSelected * tabWidth;
            if (newX < 0) {
                // Pan Izquierdo
                this.onBottomNavTap(this.defaultSelected - tabSelected, 50);
                // Cambiar el índice de pestañas seleccionado cuando se desplaza hacia la izquierda
                this.tabs.nativeElement.selectedIndex = this.defaultSelected - tabSelected;
            } else {
                // Pan derecho
                this.onBottomNavTap(this.defaultSelected + tabSelected, 50);
                // Cambiar el índice de pestañas seleccionado cuando se desplaza hacia la derecha
                this.tabs.nativeElement.selectedIndex = this.defaultSelected + tabSelected;
            }
        }
    }

    // Auxiliar de la barra de pestañas
    initializeTabBar(): void {
        // Configurar la capa base
        this.leftTabs.nativeElement.width = screen.Screen.mainScreen.widthDIPs;
        this.rightTabs.nativeElement.width = screen.Screen.mainScreen.widthDIPs;
        this.centerPatch.nativeElement.width = 100;

        this.tabBGContainer.nativeElement.translateX = - (screen.Screen.mainScreen.widthDIPs / 2) - (80 / 2);

        // Establecer pestaña seleccionada predeterminada
        const tabContentsArr = this.tabContents.toArray();
        tabContentsArr[this.defaultSelected].nativeElement.scaleX = 1.5;
        tabContentsArr[this.defaultSelected].nativeElement.scaleY = 1.5;
        tabContentsArr[this.defaultSelected].nativeElement.translateY = - 15;
        this.currentTabIndex = this.defaultSelected;
    }

    getSlideAnimation(index: number, duration: number) {
        return {
            translate: { x: this.getTabTranslateX(index), y: 0 },
            curve: this.animationCurve,
            duration: duration
        }
    }

    getFocusAnimation(index: number, duration: number) {
        return {
            scale: { x: 1.5, y: 1.5 },
            translate: { x: 0, y: -15 },
            duration: duration
        }
    }

    getUnfocusAnimation(index: number, duration: number) {
        return {
            scale: { x: 1, y: 1 },
            translate: { x: 0, y: 0 },
            duration: duration
        }
    }

    getTabTranslateX(index: number): number {
        return index * screen.Screen.mainScreen.widthDIPs / this.tabList.length - (screen.Screen.mainScreen.widthDIPs / 2) + (80 / 2);
    }

    deleteData() {
        ApplicationSettings.remove('userActive');
    }

}