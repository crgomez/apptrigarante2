import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { PoliciesClass } from '~/app/core/Classes/Policies/PoliciesClass';
import { PoliciesService } from '~/app/core/Services/Policies/policies.service';

@Component({
    selector: 'app-policies',
    templateUrl: 'policies.component.html',
    styleUrls: ['policies.component.scss']
})

export class PoliciesComponent implements OnInit {
    
    userActive: number;
    textLoader: string;
    policies: PoliciesClass = new PoliciesClass(this.policiesService);
    

    constructor(
        private policiesService: PoliciesService,
        private routerExtensions: RouterExtensions
    ) {
        this.textLoader = "Cargando pólizas";
    }

    ngOnInit() {
        this.getPolicies();
    }

    getPolicies() {
        this.policies.getPoliciesSqlite().then(() => {
            
        });

    }

    onPetitionsTap() {
        this.routerExtensions.navigate(["policies/petitions"], {
            transition: {
                name: "fade"
            }
        });
    }

    onViewTap(file: string) {
        this.routerExtensions.navigate(["policies/view", file], {
            transition: {
                name: "fade"
            }
        });
    }

    onDetailsTap(id: number) {
        this.routerExtensions.navigate(["policies/details", id], {
            transition: {
                name: "fade"
            }
        });
    }

}
