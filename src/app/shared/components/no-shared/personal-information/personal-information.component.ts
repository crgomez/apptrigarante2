import { Component, OnInit } from '@angular/core';
import { ApplicationSettings } from '@nativescript/core';
import { PersonalInformationClass } from '~/app/core/Classes/PersonalInformation/PersonalInformationClass';
import { SqliteMethodsClass } from '~/app/core/Classes/Sqlite/SqliteMethodsClass';

@Component({
    selector: 'app-personal-information',
    templateUrl: 'personal-information.component.html',
    styleUrls: ['personal-information.component.scss']
})

export class PersonalInformationComponent implements OnInit {

    dataClient: Array<[]>;
    personalInformation: PersonalInformationClass = new PersonalInformationClass();

    constructor() { }

    ngOnInit() {
        this.loadDataClient();
    }

    private loadDataClient() {
        this.personalInformation.loadDataClient()
            .then((data) => {
                this.dataClient = data;
            })
            .catch((error) => {
                console.log('Error Result Personal Information Component => ', error.message);
            })
            .finally(() => {
                console.log('Terminó de cargar la información');
            });
    }
}
