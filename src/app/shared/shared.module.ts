import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { LoaderComponent } from './components/loader/loader.component';
import { PersonalInformationComponent } from './components/no-shared/personal-information/personal-information.component';
import { PoliciesComponent } from './components/no-shared/policies/policies/policies.component';
import { TabsComponent } from './components/tabs/tabs.component';

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        LoaderComponent,
        TabsComponent
    ],
    declarations: [
        LoaderComponent,
        TabsComponent,
        // Componentes que no se compartirán
        PersonalInformationComponent,
        PoliciesComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
